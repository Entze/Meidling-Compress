{-
Copyright (c) 2016, Lukas Grassauer All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
	
	* Redistributions of source code must retain the above copyright
	  notice, this list of conditions and the following disclaimer.
	
	* Redistributions in binary form must reproduce the above copyright
	  notice, this list of conditions and the following disclaimer in the
	  documentation and/or other materials provided with the distribution.
	
	* Neither the name of Meidling-Compress nor the names of its
	  contributors may be used to endorse or promote products derived
	  from this software without specific prior written permission.


THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-}
import Mclib

import Control.Monad
import System.Environment
import System.Console.GetOpt
import System.Exit

main :: IO()
main = do
	args <- getArgs

	let (actions, nonOptions, errors) = getOpt RequireOrder options args
	
	opts <- foldl (>>=) (return defaultOptions) actions
	
	let Options { optShowHelp = wantHelp, optShowVersion = wantVersion, optShowLicense = wantLicense, optIgnoreCase = ignoreCase } = opts
	
	unless (null errors) (die ((foldl (++) [] errors) ++ "\n" ++ usageText))
	when wantHelp (help)
	when wantVersion (version)
	when wantLicense (license)
	when (null nonOptions) (die ("Did not specify any text.\n\n" ++ usageText))
	mapM_ (putStrLn) $ map compressText nonOptions

data Options = Options
	{
	optShowHelp :: Bool,
	optShowVersion :: Bool,
	optShowLicense :: Bool,
	optIgnoreCase :: Bool
	} deriving Show

defaultOptions :: Options
defaultOptions = Options
	{
	optShowHelp = False,
	optShowVersion = False,
	optShowLicense = False,
	optIgnoreCase = False
	}
		
options :: [OptDescr (Options -> IO Options)]
options = 
	[Option ['v'] ["version"]
	(NoArg
	(\opt -> return opt {optShowVersion = True}
	))
	"Print version",
	Option ['h'] ["help"]
	(NoArg
	(\opt -> return opt {optShowHelp = True}
	))
	"Show this text"
	,
	Option ['l'] ["license", "licence"]
	(NoArg
	(\opt -> return opt {optShowLicense = True}
	))
	"Show license text"
	,
	Option ['i'] ["ignore-case"]
	(NoArg
	(\opt -> return opt {optIgnoreCase = True}))
	"transforms the input into all lowercase"
	]

programNameShort :: String
programNameShort = "mc"

programNameLong :: String
programNameLong = "Meidling-Compress"


license = do
	putStrLn licenseText
	exitSuccess

licenseText :: String
licenseText = "Copyright (c) 2016, Lukas Grassauer All rights reserved." ++
	"\nRedistribution and use in source and binary forms, with or without" ++
	"\nmodification, are permitted provided that the following conditions are met:" ++
	"\n\n\t* Redistributions of source code must retain the above copyright" ++
	"\n\t  notice, this list of conditions and the following disclaimer." ++
	"\n\n\t* Redistributions in binary form must reproduce the above copyright" ++
	"\n\t  notice, this list of conditions and the following disclaimer in the" ++
	"\n\t  documentation and/or other materials provided with the distribution." ++
	"\n\n\t* Neither the name of Meidling-Compress nor the names of its" ++
	"\n\t  contributors may be used to endorse or promote products derived" ++
	"\n\t  from this software without specific prior written permission." ++
	"\n\n\nTHIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS\n" ++
	"\"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED\n" ++
	"TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A\n" ++
	"PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT\n" ++
	"HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,\n" ++
	"SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED\n" ++
	"TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR\n" ++
	"PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF\n" ++
	"LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING\n" ++
	"NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS\n" ++
	"SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."

allowedFlags :: String
allowedFlags = "hIlv"

allowedLongFlags :: String
allowedLongFlags = "[--help] [--ignore-case] [--license|licence] [--version]"

help = do
	putStrLn usageText
	exitSuccess

usageText :: String
usageText = "Usage:\t" ++ programNameShort ++ " [-" ++ allowedFlags ++ "] " ++ allowedLongFlags ++
	" text...\n\tReads input(s) and deletes consecutive letters and words.\n\t" ++
	"\n" ++ 
	"Options:\n" ++
	" -h --help\t\t\tprint this text\n" ++
	" -I --ignore-case\t\tconvert the text to all lower before compressing\n" ++
	" -v --version\t\t\tprint version\n" ++
	" -l --license --licence\t\tprint license (BSD 3-Clause)\n\n" ++
	"EXAMPLE:\n mc \"All the many many letters.\"\n Al the many leters.\n\n"
	


version = do
	putStrLn versionText
	exitSuccess

versionText :: String
versionText = programNameLong ++ " " ++ versionNumber

versionNumber :: String
versionNumber = "0.1.0"

die errorMessage = do
	putStrLn errorMessage
	exitFailure
